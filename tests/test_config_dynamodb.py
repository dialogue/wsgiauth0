import pytest


@pytest.fixture
def dynamodb():
    import boto3
    import moto
    dynamodb_mock = moto.mock_dynamodb2()
    dynamodb_mock.start()
    yield boto3.resource('dynamodb', region_name='us-east-1')
    dynamodb_mock.stop()


@pytest.fixture
def confd_table(dynamodb):
    name = 'confd'
    dynamodb.create_table(
        TableName=name,
        KeySchema=[{'AttributeName': 'service',
                    'KeyType': 'HASH'},
                   {'AttributeName': 'label',
                    'KeyType': 'RANGE'}
                   ],
        AttributeDefinitions=[],
        ProvisionedThroughput={
            'ReadCapacityUnits': 1,
            'WriteCapacityUnits': 1,
        },
    )
    return name


@pytest.fixture
def service():
    return "/service"


@pytest.fixture
def confd_table_with_config(dynamodb, confd_table, service):
    service = service
    dynamodb.batch_write_item(
        RequestItems={
            confd_table: [
                {
                    "PutRequest": {
                        "Item": {
                            "service": service,
                            "label": "A key",
                            "secret": {
                                "type": "text",
                                "value": "secret value"
                            },
                            "id": "myid",
                            "audience": "myaudience"
                        }
                    }
                }
            ]
        }
    )
    return confd_table


@pytest.fixture
def config_with_settings(confd_table_with_config, service):
    from wsgiauth0.config_dynamodb import \
        clients_config_table_key, \
        clients_config_service_key
    config = {
        clients_config_table_key: confd_table_with_config,
        clients_config_service_key: service
    }
    yield config


def test_get_records(dynamodb, confd_table_with_config, service):
    from wsgiauth0.config_dynamodb import get_records
    items = get_records(dynamodb, confd_table_with_config, service)
    for item in items:
        assert item['service'] == service


def test_get_records_table_doesnt_exist(dynamodb, service):
    from wsgiauth0.config_dynamodb import get_records
    from wsgiauth0.exception import Error
    with pytest.raises(Error):
        get_records(dynamodb, 'doesnt_exist', service)


def test_dynamodb_config_values(dynamodb):
    from wsgiauth0.config_dynamodb import \
        dynamodb_config_values, \
        clients_config_service_key, \
        clients_config_table_key
    from wsgiauth0.exception import Error

    config = {}
    with pytest.raises(Error):
        table_name, service = dynamodb_config_values(config)

    config[clients_config_table_key] = 'abc'
    with pytest.raises(Error):
        table_name, service = dynamodb_config_values(config)

    config[clients_config_service_key] = 'def'

    table_name, service = dynamodb_config_values(config)
    assert table_name == 'abc'
    assert service == 'def'


def test_read_clients(config_with_settings):
    from wsgiauth0 import read_clients
    result = read_clients(config_with_settings)
    assert result != {}
