from pytest import fixture, raises

import yaml


@fixture
def config_file(tmpdir, config_dict):
    local_path = tmpdir.join('config.yaml')
    with local_path.open('w') as f:
        yaml.dump(config_dict, f)
    return str(local_path)


@fixture
def config(config_file):
    return {'clients_config_file': config_file}


def test_client_settings_from_yaml_fails_without_clients_config_file_info():
    from wsgiauth0.config_yaml import client_settings_from_yaml
    from wsgiauth0.exception import Error

    with raises(Error) as cm:
        client_settings_from_yaml({'no': 'clients_config_file key'})

    assert 'missing_config' == cm.value.args[0]
    assert '"clients_config_file" key not found in config' \
        == cm.value.args[1]


def test_client_settings_from_yamlfails_with_bad_file():
    from wsgiauth0.config_yaml import client_settings_from_yaml
    from wsgiauth0.exception import Error

    config = {'clients_config_file': 'not-found.yml'}
    with raises(Error) as cm:
        client_settings_from_yaml(config)

    assert 'invalid_config_file' == cm.value.args[0]
    assert (
        'Not able to read yaml data from file path="%s"' % 'not-found.yml'
        == cm.value.args[1]
    )


def test_client_settings_from_yaml_fails_with_wrong_file_format(tmpdir):
    from wsgiauth0.config_yaml import client_settings_from_yaml
    from wsgiauth0.exception import Error

    path = tmpdir.join('wannabee-yaml-config.yaml')
    with path.open('w') as f:
        f.write('{_ {_ { { Not Yaml At All')

    config = {'clients_config_file': str(path)}
    with raises(Error) as cm:
        client_settings_from_yaml(config)

    assert 'invalid_config_file' == cm.value.args[0]
    assert (
        'Not able to read yaml data from file path="%s"' % str(path)
        == cm.value.args[1]
    )


def test_client_settings_from_yaml_fails_with_wrong_yaml_content(tmpdir):
    from wsgiauth0.config_yaml import client_settings_from_yaml
    from wsgiauth0.exception import Error

    path = tmpdir.join('wannabee-yaml-config.yaml')
    with path.open('w') as f:
        f.write('"valid yaml but no a dict as top level"')

    config = {'clients_config_file': str(path)}
    with raises(Error) as cm:
        client_settings_from_yaml(config)

    assert 'invalid_config_format' == cm.value.args[0]
    assert (
        'Expects a top level dict, got %s.' % str
        == cm.value.args[1]
    )


def test_read_clients_with_misding_client_config(tmpdir):
    from wsgiauth0 import read_clients
    from wsgiauth0.exception import Error

    path = tmpdir.join('wannabee-yaml-config.yaml')
    wrong_dict = {'Client': {'nope': 'No key: algorithm secret id audience'}}

    with path.open('w') as f:
        yaml.dump(wrong_dict, f)

    config = {'clients_config_file': str(path)}
    config = {'clients': wrong_dict}

    with raises(Error):
        read_clients(config)


def test_read_clients(config, clients, config_file):
    from wsgiauth0 import read_clients
    from wsgiauth0.config_yaml import clients_config_file_key

    config[clients_config_file_key] = config_file
    result = read_clients(config)

    assert clients == result
