import time

try:
    from unittest.mock import Mock
except ImportError:  # pragma no cover
    from mock import Mock

from jose import jwt
from jose.utils import base64url_encode

from pytest import fixture, raises

from wsgiauth0 import PY2


if not PY2:
    def unicode(x):
        return x


@fixture(scope='session', autouse=True)
def monkeypatch_jws_get_keys():
    from wsgiauth0 import monkeypatch_jws_get_keys
    monkeypatch_jws_get_keys()


@fixture
def config(hs256_client_config, rs256_client_config):
    client_config = hs256_client_config.copy()
    client_config.update(rs256_client_config)
    return {'clients': client_config}


@fixture
def hs256_header():
    return {
        'alg': 'HS256',
        'typ': 'JWT'
    }


@fixture
def hs256_claims(hs256_client):
    issued_at = time.time()
    expired = issued_at + 24 * 60 * 60

    return {
        'iss': hs256_client.audience,
        'sub': 'auth0|user_id',
        'aud': hs256_client.id,
        'exp': expired,
        'iat': issued_at,
    }


@fixture
def rsa_public_key():
    return (
        '-----BEGIN PUBLIC KEY-----\n'
        'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDdlatRjRjogo3WojgGHFHYLugdUWAY9'
        'iR3fy4arWNA1KoS8kVw33cJibXr8bvwUAUparCwlvdbH6dvEOfou0/gCFQsHUfQrSDv+M'
        'uSUMAe8jzKE4qW+jK+xQU9a03GUnKHkkle+Q0pX/g6jXZ7r1/xAK5Do2kQ+X5xK9cipRg'
        'EKwIDAQAB\n'
        '-----END PUBLIC KEY-----\n'
    )


@fixture
def rsa_private_key():
    return (

        '-----BEGIN RSA PRIVATE KEY-----\n'
        'MIICWwIBAAKBgQDdlatRjRjogo3WojgGHFHYLugdUWAY9iR3fy4arWNA1KoS8kVw33cJi'
        'bXr8bvwUAUparCwlvdbH6dvEOfou0/gCFQsHUfQrSDv+MuSUMAe8jzKE4qW+jK+xQU9a0'
        '3GUnKHkkle+Q0pX/g6jXZ7r1/xAK5Do2kQ+X5xK9cipRgEKwIDAQABAoGAD+onAtVye4i'
        'c7VR7V50DF9bOnwRwNXrARcDhq9LWNRrRGElESYYTQ6EbatXS3MCyjjX2eMhu/aF5YhXB'
        'wkppwxg+EOmXeh+MzL7Zh284OuPbkglAaGhV9bb6/5CpuGb1esyPbYW+Ty2PC0GSZfIXk'
        'Xs76jXAu9TOBvD0ybc2YlkCQQDywg2R/7t3Q2OE2+yo382CLJdrlSLVROWKwb4tb2PjhY'
        '4XAwV8d1vy0RenxTB+K5Mu57uVSTHtrMK0GAtFr833AkEA6avx20OHo61Yela/4k5kQDt'
        'jEf1N0LfI+BcWZtxsS3jDM3i1Hp0KSu5rsCPb8acJo5RO26gGVrfAsDcIXKC+bQJAZZ2X'
        'IpsitLyPpuiMOvBbzPavd4gY6Z8KWrfYzJoI/Q9FuBo6rKwl4BFoToD7WIUS+hpkagwWi'
        'z+6zLoX1dbOZwJACmH5fSSjAkLRi54PKJ8TFUeOP15h9sQzydI8zJU+upvDEKZsZc/UhT'
        '/SySDOxQ4G/523Y0sz/OZtSWcol/UMgQJALesy++GdvoIDLfJX5GBQpuFgFenRiRDabxr'
        'E9MNUZ2aPFaFp+DyAe+b4nDwuJaW2LURbr8AEZga7oQj0uYxcYw==\n'
        '-----END RSA PRIVATE KEY-----\n'
    )


@fixture
def rs256_token(rsa_private_key):
    header = {
        'alg': 'RS256',
        'typ': 'JWT',
    }
    claims = {
        'sub': '1234567890',
        'name': 'John Doe',
        'admin': True
    }

    return jwt.encode(claims, rsa_private_key, 'RS256', header)


@fixture
def hs256_token(hs256_claims, hs256_secret, hs256_header):
    return jwt.encode(hs256_claims, hs256_secret, 'HS256', hs256_header)


@fixture
def hs256_authorization(hs256_token):
    return 'Bearer %s' % hs256_token


@fixture
def encode_jwt():

    def encode(
        claims,
        key,
        headers=None,
        algorithm='HS256',
        is_base64_encoded=True
    ):
        key = base64url_encode(key) if is_base64_encoded else key
        return jwt.encode(claims, key, headers=headers, algorithm=algorithm)

    return encode


def test_validate_hs256_jwt_claims(
    clients,
    hs256_authorization,
    hs256_client,
    hs256_claims,
    hs256_token,
):
    from wsgiauth0 import validate_jwt_claims
    jwt_environ = validate_jwt_claims(clients, hs256_authorization)

    assert hs256_claims == jwt_environ['wsgiauth0.jwt_claims']
    assert jwt_environ['wsgiauth0.jwt_error'] is None
    assert jwt_environ['wsgiauth0.jwt_token'] == hs256_token
    client = hs256_client._asdict()
    client.pop('secret')
    assert client == jwt_environ['wsgiauth0.jwt_auth0_client']


def test_invalidate_with_authorization_being_none(clients):
    from wsgiauth0 import validate_jwt_claims

    jwt_environ = validate_jwt_claims(clients, None)

    assert jwt_environ['wsgiauth0.jwt_claims'] is None
    assert jwt_environ['wsgiauth0.jwt_auth0_client'] is None
    assert jwt_environ['wsgiauth0.jwt_error'] is not None


def test_validate_with_invalid_token(clients):
    from wsgiauth0 import validate_jwt_claims
    jwt_environ = validate_jwt_claims(clients, 'NotBearer whatever')

    assert jwt_environ['wsgiauth0.jwt_claims'] is None
    assert jwt_environ['wsgiauth0.jwt_auth0_client'] is None
    assert jwt_environ['wsgiauth0.jwt_error'] is not None

    error = jwt_environ['wsgiauth0.jwt_error']
    assert 'code' in error
    assert 'description' in error


def test_validate_with_invalid_token_algorithm(clients, rs256_token):
    from wsgiauth0 import validate_jwt_claims
    jwt_environ = validate_jwt_claims(clients, 'Bearer whatever')

    assert jwt_environ['wsgiauth0.jwt_claims'] is None
    assert jwt_environ['wsgiauth0.jwt_auth0_client'] is None
    assert jwt_environ['wsgiauth0.jwt_error'] is not None

    error = jwt_environ['wsgiauth0.jwt_error']
    assert 'code' in error
    assert 'description' in error


def test_extract_token_with_authorization_without_any_space():
    from wsgiauth0 import Error, extract_token

    with raises(Error) as cm:
        extract_token('no-space-authorization')

    assert 'invalid_header' == cm.value.args[0]
    assert (
        'Authorization header must be "Bearer token".' == cm.value.args[1]
    )


def test_extract_token_with_authorization_with_too_many_spaces():
    from wsgiauth0 import Error, extract_token

    with raises(Error) as cm:
        extract_token('way too much-spaces')

    assert 'invalid_header' == cm.value.args[0]
    assert (
        'Authorization header must be "Bearer token".' == cm.value.args[1]
    )


def test_extract_token_with_authorization_not_starting_by_bearer():
    from wsgiauth0 import Error, extract_token

    with raises(Error) as cm:
        extract_token('not-Bearer akey')

    assert 'invalid_header' == cm.value.args[0]
    assert (
        'Authorization header must start with "Bearer".' == cm.value.args[1]
    )


def test_extract_token_that_looks_valid():
    from wsgiauth0 import extract_token
    assert 'token' == extract_token('Bearer token')


def test_extract_client_with_an_invalid_token(clients):
    from wsgiauth0 import Error, extract_client

    with raises(Error) as cm:
        extract_client(clients, 'that is not a valid token')

    assert 'invalid_token' == cm.value.args[0]
    assert 'Error decoding token claims.' == cm.value.args[1]


def test_extract_client_with_no_audience_in_claims(
    clients,
    encode_jwt,
    hs256_secret,
):
    from wsgiauth0 import Error, extract_client

    token = encode_jwt({'no': 'aud key'}, hs256_secret)
    with raises(Error) as cm:
        extract_client(clients, token)

    assert 'invalid_claims' == cm.value.args[0]
    assert 'No key aud in claims.' == cm.value.args[1]


def test_extract_client_with_no_subject_in_claims(
    clients,
    encode_jwt,
    hs256_secret,
):
    from wsgiauth0 import Error, extract_client

    token = encode_jwt({'aud': 'audience', 'no': 'sub key'}, hs256_secret)
    with raises(Error) as cm:
        extract_client(clients, token)

    assert 'invalid_claims' == cm.value.args[0]
    assert 'No key sub in claims.' == cm.value.args[1]


def test_extract_client_with_missing_client_config(
    clients,
    encode_jwt,
    hs256_secret,
):
    from wsgiauth0 import Error, extract_client

    token = encode_jwt({'aud': 'audience', 'sub': 'subject'}, hs256_secret)
    with raises(Error) as cm:
        extract_client(clients, token)

    assert 'invalid_client' == cm.value.args[0]
    assert 'No config found for this client.' == cm.value.args[1]


def test_extract_client_with_aud_as_clients_key(
    clients,
    hs256_client,
    hs256_token,
):
    from wsgiauth0 import extract_client

    client = extract_client(clients, hs256_token)
    assert client == hs256_client


def test_read_clients_with_no_config():
    from wsgiauth0 import read_clients
    from wsgiauth0.exception import Error

    with raises(Error) as cm:
        read_clients({})

    assert 'missing_config' == cm.value.args[0]
    assert 'No auth0 clients configured' == cm.value.args[1]


def test_read_clients_with_missing_client_config():
    from wsgiauth0 import read_clients
    from wsgiauth0.exception import Error

    wrong_dict = {'Client': {'nope': 'No key: algorithm secret id audience'}}

    config = {'clients': wrong_dict}
    with raises(Error) as cm:
        read_clients(config)

    assert 'missing_config_key' == cm.value.args[0]
    assert 'Client config missing key client_dict.' == cm.value.args[1]


def test_factory(config):
    from wsgiauth0 import factory

    def application(environ, start_response):  # pragma: no cover
        pass

    app = factory(application, config=None, **config)

    assert callable(app)


def test_auth0_middleware(
    config,
    hs256_authorization,
    hs256_client,
    hs256_claims,
    hs256_token,
):
    """Check nominal behaviour of middleware."""
    from wsgiauth0 import auth0_middleware

    environ = {'HTTP_AUTHORIZATION': hs256_authorization}
    start_response = Mock()
    application = Mock()

    app = auth0_middleware(application, config)

    app(environ, start_response)

    assert application.called is True
    assert application.call_count == 1

    updated_environ, start_response = application.call_args[0]

    assert 6 == len(updated_environ.keys())

    assert hs256_token == updated_environ['wsgiauth0.jwt_token']
    assert hs256_authorization == updated_environ['HTTP_AUTHORIZATION']
    assert hs256_claims['sub'] == updated_environ['REMOTE_USER']

    assert updated_environ['wsgiauth0.jwt_error'] is None
    client = hs256_client._asdict()
    client.pop('secret')
    assert (
        updated_environ['wsgiauth0.jwt_auth0_client']
        == client
    )
    assert updated_environ['wsgiauth0.jwt_claims'] == hs256_claims


def test_parse_client(hs256_client_config):
    from wsgiauth0 import parse_client
    for client_config in hs256_client_config.items():
        client = parse_client(client_config)
        assert client


def test_parse_client_unicode_secret(hs256_client_config):
    from wsgiauth0 import parse_client, PY2
    for k, v in hs256_client_config.items():
        if PY2:
            v['secret']['value'] = unicode(v['secret']['value'])
        client = parse_client((k, v))
        assert client


def test_parse_clients_return_map_with_client_id_as_keys(hs256_client_config):
    from wsgiauth0 import parse_clients
    clients = parse_clients(hs256_client_config)
    assert clients['hs256_client_id']
