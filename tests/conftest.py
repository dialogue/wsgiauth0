import logging

from jose.utils import base64url_decode, base64url_encode

from pytest import fixture


@fixture(scope='session', autouse=True)
def setup_logging():
    logging.basicConfig(level='DEBUG')
    logging.getLogger('root').setLevel('INFO')


@fixture
def hs256_secret():
    return base64url_decode('hs256_secret')


@fixture
def rs256_secret():
    # challenge: 'challenge'
    return (
        '-----BEGIN CERTIFICATE REQUEST-----'
        'MIIBZjCB0AIBADANMQswCQYDVQQGEwJDQTCBnzANBgkqhkiG9w0BAQEFAAOBjQAw'
        'gYkCgYEAx2LwsUexPKQ/0GIHqugXZtIGZxSOovO754KWn3ZWBbDvm/wuh+QfmMj8'
        'ZTxnxRymHjSNJ04nCMcqtzl3VDwapMkM433CnyZjoJjA/fRwLRjUepLAMbmoqkOG'
        'k1BKNAyidyko7DBnkMayzJRfmnCwFy1hsuikh6oFSinU7MP3LBsCAwEAAaAaMBgG'
        'CSqGSIb3DQEJBzELEwljaGFsbGVuZ2UwDQYJKoZIhvcNAQELBQADgYEAP819zy3q'
        '1gh5z5FLeFanc3TpdlcGHCQxcTMC/x9iyMpbSd2XkKLrZ02Is1Y8Ox/XeT8zNjOg'
        '/nulPg6YrIsywpKFR4orMvuUUMZ8uT8UVNj1pnatmXy9ikjdGtBXeU+EKkMZ4q6a'
        'OrG8qyB4o/WETphyxfneazWt3jrLHkKBvXA='
        '-----END CERTIFICATE REQUEST-----'
    )


@fixture
def hs256_client_config(hs256_secret):
    return {
        'hs256 client label': {
            'id': 'hs256_client_id',
            'audience': 'hs256_client_id',
            'secret': {
                'value': base64url_encode(hs256_secret),
                'type': 'base64_url_encoded',
            }
        },
    }


@fixture
def hs256_client(hs256_client_config, hs256_secret):
    from wsgiauth0 import Client, Secret
    settings = hs256_client_config['hs256 client label'].copy()
    settings.update(label='hs256 client label')
    settings.pop('secret')
    s = Secret(value=hs256_secret, type='base64_url_encoded')
    return Client(secret=s, **settings)


@fixture
def rs256_client_config(rs256_secret):
    return {
        'rs256 client label': {
            'id': 'rs256_client_id',
            'audience': 'rs256_client_id',
            'secret': {
                'value': rs256_secret,
                'type': 'certificate',
            }
        },
    }


@fixture
def rs256_client(rs256_client_config, rs256_secret):
    from wsgiauth0 import Client, Secret
    settings = rs256_client_config['rs256 client label'].copy()
    settings.update(label='rs256_client_id')
    settings.pop('secret')
    s = Secret(value=rs256_secret, type='certificate')
    return Client(secret=s, **settings)


@fixture
def clients(hs256_client, rs256_client):
    return {hs256_client.id: hs256_client, rs256_client.id: rs256_client}


@fixture
def config_dict(clients):
    """Structure of config file."""
    config_dict = {}
    for key, client in clients.items():

        if client.secret.type == 'base64_url_encoded':
            secret_value = base64url_encode(client.secret.value)
        else:
            secret_value = client.secret.value

        config_dict[client.label] = {
            'id': client.id,
            'audience': client.audience,
            'secret': {
                'type': client.secret.type,
                'value': secret_value,
            },
        }
    return config_dict
